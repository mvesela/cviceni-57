/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-dynamic-require */
// dependecies
const { writeFile } = require('fs');
const { existsSync, mkdirSync } = require('fs');

const catalogFolder = 'dist/catalogs/';

const json = require(`${process.cwd()}/tmp/data/cviceni.json`);
const exerciseNames = [...Object.keys(json)];

// convert JSON to array
// - creates array of key (name of the object, @String), value (object, @Object) pairs if contains catalog value
const getExercises = (exerciseName, catalog) => {
  if (json[exerciseName].cviceni.katalog && json[exerciseName].cviceni.katalog.includes(catalog)) {
    return [
      exerciseName, json[exerciseName],
    ];
  }

  return false;
};

const catalogs = {
  public: exerciseNames.map((exerciseName) => getExercises(exerciseName, 'public')).filter((exercise) => exercise),
  test: exerciseNames.map((exerciseName) => getExercises(exerciseName, 'test')).filter((exercise) => exercise),
  staging: [],
};

catalogs.staging = [...catalogs.public, ...catalogs.test];

// loop object (catalogs)
for (const catalog in catalogs) {
  if (Object.prototype.hasOwnProperty.call(catalogs, catalog)) {
    // possible from Node 12.0.0
    // const data = Object.fromEntries(catalogs[catalog]);

    const data = {};

    // loop exercies in an array
    catalogs[catalog].forEach((exercise) => {
      // save data
      // property: is name of file
      // value: is loaded data
      data[exercise[0]] = {
        cviceni: exercise[1].cviceni,
      };
    });

    if (Object.keys(data).length > 0) {
      // check if directory exists
      if (!existsSync(catalogFolder)) {
        mkdirSync(catalogFolder);
      }

      // get name of the file
      const fileName = `${catalog}.json`;

      // create JSON file for catalog
      writeFile(`${catalogFolder}${fileName}`, JSON.stringify(data), 'utf8', (error) => {
        if (error) throw error;
        console.log(`✓ ${catalogFolder}${catalog}.json has been saved with`, Object.keys(data).length, 'entries!');
      });

    } else {
      console.warn(`${catalog} does not have any entries!`);
    }
  }
}
